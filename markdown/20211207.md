---
Title: Windows 10 伺服器
Date: 2021-12-07 11:00
Category: KMOLab
Tags: 202112
Slug: 2021-windows-10-server
Author: yen
---

你沒有看錯, 這篇網誌的標題是: Windows 10 伺服器, 而且是適合用於多人協同模式下的機電資整合產品開發伺服器.

<!-- PELICAN_END_SUMMARY -->

[KMOLab] 現在進行教學與研究的需求, 是一條通暢的網路線, 以及一台可以開機的 64位元電腦. 五年前剛剛被淘汰下來的電腦, 非常適合, 即便用了 10 年被淘汰下來的電腦, 沒有獨立顯卡, 也不支援 VT-x 或 VT-d 的老舊電腦也沒有關係, 通通可以當作伺服器.

假如您知道一個 32 GB 的 USB 3.0 隨身碟就能夠容納用來編譯 Solvespace, CoppiliaSim, Range3 以及 Webots, 並且帶有完整的 Flutter, Rust, Deno, Elixir 以及 Python 等網際前後端可攜程式開發環境. 其實用於值得開發的機電資專案, 所需要的資源就是一條網路線與可以開機的 64 位元電腦. 當然, 前提是: 必須運用校方所購買的線上電子書與文獻, 各種開源或免費資源, 且具備自行開發所需的工具的動機與能力.

五年前的電腦
----

2021 年  10 月份預計從八台五年新的電腦組成至少四台伺服器, 前端使用 Windows 10 只支援 IPv6 網址, 而虛擬主機則採 Ubuntu, 所使用的工具有 nginx, stunnel, Fossil SCM, Flask 程式以及 LetsEncrypt 服務.

因為系上提供 2017 年的 NX12 以及 2019 年的 NX1872, 因為是少數能夠製作成可攜系統的 MCAD 套件, 因此也配置在這一批電腦上.

專題應該怎麼做?
----

從起始的第一天起, 就應該配置一個 Fossil SCM 倉儲並且設法與 Github, Gitlab 與 Bitbucket 倉儲同步, 與專題相關的大檔案則從系上或院的 Google Workspace 所配發的 GDrive 上, 且專題結案時, 統一關閉 G Workspace 上的帳號擷取權限, 讓專題流程與內容成為 Fossil 內容, 只能查詢無法改版.

與專題相關的網頁, 則配置在 Github Pages 且同步至 Fossil SCM Documentation 網頁. 專題簡報也必須採網際方式進行.

每一個年度則統一整理在特定網頁, 讓爾後的升學或求職都能透過學號回系上或院查詢該生在課程或專題流程中的細節.

Windows 10 
----

在使用 Windows 10 時經常聽到的揶揄是: Edge 的最大功能是用來安裝 Firefox.

現在為了能在 Windows 10 有兩個能自行設定代理主機的瀏覽器, 除了 Firefox 還必須安裝 Waterfox, 至於 Chrome 已經不是早年不為惡公司 Google 的產品, 在這些所謂的伺服器電腦上並不會安裝. 

假如設定 http 連線的 Windows 10 系統代理主機, 在設定 LetsEncrypt 流程會產品 SSL error, 因此 Windows 系統安裝好之後, 第一件有關網路設定的工作就是關閉 Windows Proxy 自動偵測以及設定. 此後個別的網路服務都必須自行設定 Proxy server, 除了瀏覽器, 來包括 git, pip, heroku, ssh 等工具的網路連線.



[KMOLab]: https://mde.tw
